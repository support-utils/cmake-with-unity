#include <stdbool.h>
#include <unity.h>

#include "template.h"

void test_always_successful(void) { TEST_ASSERT_TRUE(true); }

void test_false_successful(void) { TEST_ASSERT_FALSE(false); }

void test_template(void) { TEST_ASSERT_FALSE(Template_Invert(true)); }