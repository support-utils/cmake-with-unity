# Specify specifically for ARM.
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)

# Make use of C99.
set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS OFF)

if(ARM_COMPILER)
  # Use arm-none-eabi and set the newlib library flag.
  set(CMAKE_C_COMPILER arm-none-eabi-gcc)
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_C_FLAGS} --specs=nosys.specs" CACHE INTERNAL "")
else()
  set(CMAKE_C_COMPILER gcc)
  # Add flags for coverage.
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -g -O0 --coverage")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage")
endif()


set(CMAKE_C_OUTPUT_EXTENSION_REPLACE ON)
